
FROM ubuntu:focal

RUN apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get -y upgrade && \
    DEBIAN_FRONTEND=noninteractive apt-get -y install gpg software-properties-common curl

RUN apt-key adv --keyserver pool.sks-keyservers.net --recv-keys ED75B5A4483DA07C && \
    apt-add-repository 'deb http://repo.aptly.info/ squeeze main' && \
    apt-get update && \
    DEBIAN_FRONTEND=noninteractive apt-get install aptly

ENV GNUPGHOME /gpg
RUN mkdir $GNUPGHOME && gpg --no-default-keyring --keyring trustedkeys.gpg --keyserver pool.sks-keyservers.net --recv-keys 3B4FE6ACC0B21F32 871920D1991BC93C 9165938D90FDDD2E 82B129927FA3303E
